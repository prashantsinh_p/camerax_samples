package com.example.myapplication;

import static androidx.camera.view.CameraController.IMAGE_ANALYSIS;
import static androidx.camera.view.CameraController.IMAGE_CAPTURE;

import android.media.FaceDetector;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.util.Size;
import android.view.OrientationEventListener;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.camera2.Camera2Config;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.CameraXConfig;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.ImageProxy;
import androidx.camera.core.Preview;
import androidx.camera.view.LifecycleCameraController;
import androidx.camera.view.PreviewView;
import com.google.common.util.concurrent.ListenableFuture;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.ExecutionException;


public class CameraActivity extends AppCompatActivity implements CameraXConfig.Provider {
    private PreviewView previewView;
    private ImageView captureImage;
    private ListenableFuture<ProcessCameraProvider> cameraProviderFuture;
    private TextView textView;
    private static final String TAG = "PDEBUG:";

    private int mOrientation = 0;
    LifecycleCameraController cameraControl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        previewView = findViewById(R.id.previewView);
        cameraControl = new LifecycleCameraController(this);
        cameraControl.setEnabledUseCases(IMAGE_CAPTURE|IMAGE_ANALYSIS);

        cameraControl.bindToLifecycle(this);

        captureImage = findViewById(R.id.captureImg);
        cameraProviderFuture = ProcessCameraProvider.getInstance(this);
        textView = findViewById(R.id.orientation);

        cameraControl.getInitializationFuture().addListener(new Runnable() {
            @Override
            public void run() {
                previewView.setController(cameraControl);
                bindImageAnalysis();

            }
        }, ContextCompat.getMainExecutor(this));

    }


    private void bindImageAnalysis() {
        Log.e(TAG, "bindImageAnalysis");
        cameraControl.setImageAnalysisAnalyzer(ContextCompat.getMainExecutor(this), new ImageAnalysis.Analyzer() {
            @Override
            public void analyze(@NonNull ImageProxy imageProxy) {
                Log.e(TAG, "analyze");

                if(mOrientation >= 0 && mOrientation <= 180) {
                    cameraControl.enableTorch(true);
                } else {
                    cameraControl.enableTorch(false);
                }
                imageProxy.close();
//                 image.setCropRect();
//                image.close();
            }
        });
        OrientationEventListener orientationEventListener = new OrientationEventListener(this) {
            @Override
            public void onOrientationChanged(int orientation) {
                mOrientation = orientation;
                textView.setText(Integer.toString(orientation));
            }
        };
        orientationEventListener.enable();
//        Preview preview = new Preview.Builder().build();
//        preview.setSurfaceProvider(previewView.getSurfaceProvider());

        previewView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
                File file = new File(getBatchDirectoryName(), mDateFormat.format(new Date()) + ".jpg");

                ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(file).build();
                Toast.makeText(CameraActivity.this, getBatchDirectoryName(), Toast.LENGTH_SHORT).show();

//                cameraControl.setTapToFocusEnabled(true);
                cameraControl.takePicture(outputFileOptions, getMainExecutor(), new ImageCapture.OnImageSavedCallback(){
                    @Override
                    public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                        new Handler().post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(CameraActivity.this, "Image saved successfully", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onError(@NonNull ImageCaptureException error) {
                        Toast.makeText(CameraActivity.this, "Image failed", Toast.LENGTH_SHORT).show();

                        error.printStackTrace();
                    }
                });

            }
        });

    }
    public String getBatchDirectoryName() {
        String app_folder_path = "";
        app_folder_path = "/storage/emulated/0/DC";
        File dir = new File(app_folder_path);
        if(!dir.exists() && !dir.mkdirs()) {
            Toast.makeText(CameraActivity.this, "Not able to create dir", Toast.LENGTH_SHORT).show();

        }
        return app_folder_path;
    }

    @Override
    public CameraXConfig  getCameraXConfig () {
        return Camera2Config.defaultConfig();
    }

}